package web.model.dao;

/**
 * Created by Artem on 13.12.2016.
 */
public interface UserDAO {
    boolean isAdmin(String username);
}
