package web.model.dao;

import web.model.actors.Book;
import web.model.actors.Comment;

import java.util.List;

/**
 * Created by Artem on 02.12.2016.
 */
public interface BookDao {
    List<Book> getAllBooks();

    List<Book> searchBooks(String search);

    Book getBookById(String id);


    void addBook(String name, String author, String category, String description, String link);
    void updateBook(int bookId, String name, String author, String category, String description, String link);
}
