package web.model.daoimpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import web.model.actors.Book;
import web.model.actors.Comment;
import web.model.dao.BookDao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Artem on 02.12.2016.
 */
@Repository
public class BookDaoImpl implements BookDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Book> getAllBooks() {
        Session session = sessionFactory.getCurrentSession();
        List<Book> books = session.createQuery("from Book").list();
        return books;
    }

    @Override
    public List<Book> searchBooks(String search) {
        Session session = sessionFactory.getCurrentSession();
        List<Book> books = session.createQuery("from Book b where b.category in :serchCategories or b.author in :serchCategories or b.name in :serchCategories").setParameter("serchCategories",search).list();
        return books;
    }

    @Override
    public Book getBookById(String id) {
        Session session  = sessionFactory.getCurrentSession();
        Book book = (Book) session.createQuery("from Book where id=:id").setParameter("id",Integer.parseInt(id)).uniqueResult();
        return book;
    }

    @Override
    public void addBook(String name, String author, String category, String description, String link) {
        Session session = sessionFactory.openSession();
        Book book = new Book();
        book.setName(name);
        book.setCategory(category);
        book.setDescription(description);
        book.setAuthor(author);
        book.setLink(link);
        session.save(book);
    }
    @Override
    public void updateBook(int bookId, String name, String author, String category, String description, String link) throws NullPointerException {
        Session session = sessionFactory.getCurrentSession();
        Book book = (Book) session.createQuery("from Book where id = :id").setParameter("id",bookId).uniqueResult();
        book.setName(name);
        book.setCategory(category);
        book.setDescription(description);
        book.setAuthor(author);
        book.setLink(link);
    }


}
