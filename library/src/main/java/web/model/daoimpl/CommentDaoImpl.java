package web.model.daoimpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import web.model.actors.Book;
import web.model.actors.Comment;
import web.model.dao.CommentDAO;

import java.util.HashSet;
import java.util.List;

/**
 * Created by Artem on 13.12.2016.
 */
@Repository
public class CommentDaoImpl implements CommentDAO {
    @Autowired
    private SessionFactory sessionFactory;
    @Override
    public void createComment(String id,String content,String userName) {
        Session session  = sessionFactory.openSession();
        Book book = (Book) session.createQuery("from Book where id=:id").setParameter("id",Integer.parseInt(id)).uniqueResult();
        Comment comment = new Comment();
        comment.setBook(book);
        comment.setContent(content);
        comment.setUserName(userName);
        book.getComments().add(comment);
        session.save(comment);
    }

    @Override
    public List<Comment> getComments(String id) {
        Session session = sessionFactory.openSession();
        Book book = (Book) session.createQuery("from Book where id=:id").setParameter("id",Integer.parseInt(id)).uniqueResult();
        List<Comment> comments = session.createQuery("from Comment where book=:book")
                .setParameter("book",book).list();
        return comments;
    }

    @Override
    public void deleteBookComments(int bookId) {
        Session session = sessionFactory.getCurrentSession();
        Book book = (Book) session.createQuery("from Book where id=:id")
                .setParameter("id",bookId).uniqueResult();
        Query query = session.createQuery("delete Comment where book=:book").setParameter("book",book);
        query.executeUpdate();
    }
}
