package web.model.daoimpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import web.model.actors.Book;
import web.model.actors.User;
import web.model.dao.UserDAO;

import java.util.List;

/**
 * Created by Artem on 13.12.2016.
 */
@Repository
public class UserDaoImpl implements UserDAO {
    @Autowired
    private SessionFactory sessionFactory;
    @Override
    public boolean isAdmin(String username) {
        Session session = sessionFactory.getCurrentSession();
        User user = (User) session.createQuery("from User where login = :username")
                .setParameter("username",username).uniqueResult();
        if(user.getRole().equals("ROLE_ADMIN")){
            return true;
        }else {
            return false;
        }
    }
}
