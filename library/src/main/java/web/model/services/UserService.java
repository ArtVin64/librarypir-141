package web.model.services;

/**
 * Created by Artem on 13.12.2016.
 */
public interface UserService {
    boolean isAdmin(String username);

}
