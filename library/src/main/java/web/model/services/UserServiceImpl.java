package web.model.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import web.model.dao.UserDAO;

/**
 * Created by Artem on 13.12.2016.
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDAO userDAO;

    @Override
    @Transactional
    public boolean isAdmin(String username) {
        return userDAO.isAdmin(username);
    }
}
