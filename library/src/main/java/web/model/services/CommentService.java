package web.model.services;

import web.model.actors.Comment;

import java.util.List;

/**
 * Created by Artem on 13.12.2016.
 */
public interface CommentService {
    void createComment(String id, String content, String remoteUser);
    List<Comment> getComments(String id);

    void deleteBookComments(int bookId);
}
