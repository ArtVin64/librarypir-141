package web.model.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import web.model.actors.Comment;
import web.model.dao.CommentDAO;

import java.util.List;

/**
 * Created by Artem on 13.12.2016.
 */
@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentDAO commentDAO;

    @Override
    public void createComment(String id, String content, String remoteUser) {
      commentDAO.createComment(id,content,remoteUser);
    }

    @Override
    public List<Comment> getComments(String id) {
        return commentDAO.getComments(id);
    }

    @Override
    @Transactional
    public void deleteBookComments(int bookId) {
        commentDAO.deleteBookComments(bookId);
    }
}
