package web.model.services;

import web.model.actors.Book;
import web.model.actors.Comment;

import java.util.List;

/**
 * Created by Artem on 02.12.2016.
 */
public interface BookService {
    List<Book> getAllBooks();

    List<Book> getAndSearchBooks(String search);

    Book getBookById(String id);


    void addBook(String name, String author, String category, String description, String link);
    void updateBookByID(int bookId, String name, String author, String category, String description, String link);
}
