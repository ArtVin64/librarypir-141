package web.model.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import web.model.actors.Book;
import web.model.actors.Comment;
import web.model.dao.BookDao;

import java.util.List;

/**
 * Created by Artem on 02.12.2016.
 */
@Service
public class BookServiceImpl implements BookService {
    @Autowired
    private BookDao bookDao;
    @Override
    @Transactional
    public List<Book> getAllBooks() {
        return bookDao.getAllBooks();
    }

    @Override
    @Transactional
    public List<Book> getAndSearchBooks(String search) {
        return bookDao.searchBooks(search);
    }

    @Override
    @Transactional
    public Book getBookById(String id) {
        return bookDao.getBookById(id);
    }

    @Override
    public void addBook(String name, String author, String category, String description, String link) {
        bookDao.addBook(name,author,category,description,link);
    }

    @Override
    @Transactional
    public void updateBookByID(int bookId, String name, String author, String category, String description, String link) throws NullPointerException {
        bookDao.updateBook(bookId,name,author,category,description,link);
    }


}
