package web.model.actors;

import javax.persistence.Column;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Artem on 02.12.2016.
 */
public class Book {
    private int id;
    private String name;
    private String category;
    private String description;
    private String link;
    private Set<Comment> comments =  new HashSet<Comment>(0);
    private String author;



    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", description='" + description + '\'' +
                ", links='" + link + '\'' +
                ", author='" + author + '\'' +
                ", comments=" + comments+
                '}';
    }


}
