package web.model;

import org.springframework.stereotype.Component;

/**
 * Created by Artem on 06.12.2016.
 */
@Component
public class SearchQuery {
    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    String query;
}
