package web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import web.model.services.BookService;

import javax.persistence.criteria.CriteriaBuilder;

/**
 * Created by Artem on 13.12.2016.
 */
@Controller
public class UpdateBookController {
    @Autowired
    private BookService bookService;
    @RequestMapping(value = "/books/update", method = RequestMethod.GET)
    public String updateBookPage(ModelMap model) {
        return "updateBook";
    }

    @RequestMapping(value = "/updateBook", method = RequestMethod.POST)
    public String addBook(ModelMap model, @RequestParam("id") String bookId,@RequestParam("name") String name, @RequestParam("author") String author,
                          @RequestParam("category") String category  , @RequestParam("description") String description, @RequestParam("link") String link) {
        if(bookId.isEmpty()||name.isEmpty()||author.isEmpty()||category.isEmpty()||description.isEmpty()){
            return "redirect:books/update";
        }
        try {
            bookService.updateBookByID(Integer.parseInt(bookId), name, author, category, description, link);
        }catch (NullPointerException e){
            return "redirect:books/update";
        }
        return "redirect:/books";
    }
}
