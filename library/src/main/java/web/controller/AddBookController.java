package web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import web.model.services.BookService;

/**
 * Created by Artem on 13.12.2016.
 */
@Controller
public class AddBookController {
    @Autowired
    private BookService bookService;
    @RequestMapping(value = "/books/add", method = RequestMethod.GET)
    public String addBookPage(ModelMap model) {
        return "addBook";
    }

    @RequestMapping(value = "/addBook", method = RequestMethod.POST)
    public String addBook(ModelMap model,@RequestParam("name") String name,@RequestParam("author") String author,
                          @RequestParam("category") String category  ,@RequestParam("description") String description,@RequestParam("link") String link) {
        if(name.isEmpty()||author.isEmpty()||category.isEmpty()||description.isEmpty()){
            return "redirect:books/add";
        }

        bookService.addBook(name,author,category,description,link);
        return "redirect:/books";
    }

}
