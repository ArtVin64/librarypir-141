package web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import web.model.services.BookService;
import web.model.services.CommentService;
import web.model.services.UserService;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Artem on 13.12.2016.
 */
@Controller
public class BookPageController {
    @Autowired
    private BookService bookService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/books/{id}", method = RequestMethod.GET)
    public String bookPage(ModelMap model, @PathVariable("id") String id,HttpServletRequest request) {
        model.put("isAdmin",userService.isAdmin(request.getRemoteUser()));
        model.put("book",bookService.getBookById(id));
        model.put("comments",commentService.getComments(id));

        return "bookPage";
    }
    @RequestMapping(value = "/comment/{id}", method = RequestMethod.POST)
    public String leaveComment(ModelMap model, @PathVariable("id") String id, @RequestParam("content") String content, HttpServletRequest request) {
        commentService.createComment(id,content,request.getRemoteUser());
        return "redirect:/books/"+id;
    }

    @RequestMapping(value = "/deleteComments/{bookId}", method = RequestMethod.GET)
    public String deleteComment(ModelMap model,@PathVariable("bookId") String bookId) {
        commentService.deleteBookComments(Integer.parseInt(bookId));
        return "redirect:/books/"+bookId;
    }
}
