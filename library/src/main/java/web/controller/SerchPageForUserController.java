package web.controller;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import web.model.SearchQuery;
import web.model.actors.Book;
import web.model.actors.Comment;
import web.model.actors.User;
import web.model.services.BookService;
import web.model.services.CommentService;
import web.model.services.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Artem on 02.12.2016.
 */
@Controller
public class SerchPageForUserController {
    @Autowired
    private BookService bookService;
    @Autowired
    private   CommentService commentService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/books", method = RequestMethod.GET)
    public String searchPage(ModelMap model,HttpServletRequest request) {
        model.put("isAdmin",userService.isAdmin(request.getRemoteUser()));
        SearchQuery searchQuery = (SearchQuery) request.getSession().getAttribute("searchQuery");
        if (searchQuery != null) {
            if(searchQuery.getQuery().isEmpty()) {
                model.put("books", bookService.getAllBooks());
            }else {
                model.put("books", bookService.getAndSearchBooks(searchQuery.getQuery()));
            }
        }else {
            model.put("books", bookService.getAllBooks());
        }
        return "books";
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String searcher(ModelMap model, @RequestParam("search") final String search,HttpServletRequest request) {
        SearchQuery searchQuery = new SearchQuery();
        searchQuery.setQuery(search);
        request.getSession().setAttribute("searchQuery",searchQuery);
        return "redirect:/books";
    }


    @RequestMapping(value = "/404", method = RequestMethod.GET)
    public String error404(ModelMap model) {
        return "redirect:/books";
    }
    @RequestMapping(value = "/deniedAccess", method = RequestMethod.GET)
    public String accessDenied(ModelMap model,HttpServletRequest request) {
        return "deniedAccess";
    }
}
